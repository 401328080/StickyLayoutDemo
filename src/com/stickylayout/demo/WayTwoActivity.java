package com.stickylayout.demo;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.ScrollView;
import android.widget.TextView;

import com.stickylayout.demo.util.PhoneUtils;

/**
 * Author: dengdayi
 * Date:2016/1/11 16:04
 * Description:
 */
public class WayTwoActivity extends Activity {
    private ScrollView mScrollView;

    private View mView;

    private TextView mTvSticky;

    private int screenHeight;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_way_two);

        mScrollView = (ScrollView) findViewById(R.id.scrollview);

        screenHeight = PhoneUtils.getHeight(this);

        ViewConfiguration.getScrollBarFadeDuration();

        initView();
    }


    private void initView() {
        mView = findViewById(R.id.header_view);

        mTvSticky = (TextView) findViewById(R.id.tv_textView);

        mScrollView.setOnTouchListener(mTouchListener);
    }

    private View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            Log.e("TAG", "mTvSticky  top:" + mTvSticky.getTop());
            float mStartY = 0f;
            float mEndY;
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    mStartY = event.getY();
                    break;

                case MotionEvent.ACTION_MOVE:
                case MotionEvent.ACTION_UP:
                    mEndY = event.getY();
                    float mScrollY = mEndY - mStartY;
                    if (mScrollY > 12) {
                        Log.e("TAG", "mScrollY:" + mScrollY);
                        mView.setTranslationY(mScrollY);
                        if (mScrollY >= mView.getHeight()) {
                            mView.setVisibility(View.GONE);
                        }
                    }
                    break;
            }
            return false;
        }
    };
}
