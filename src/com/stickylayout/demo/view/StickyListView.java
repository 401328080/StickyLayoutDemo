package com.stickylayout.demo.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

import com.stickylayout.demo.R;

/**
 * Author: dengdayi
 * Date:2016/1/11 16:04
 * Description:
 */
public class StickyListView extends ListView {
    /**
     * 头部View
     */
    private View mHeadView;
    /**
     * 最小的Head高度
     */
    private int mMinHeaderHeight;
    /**
     * ListView的头部View
     */
    private View mListViewHeaderView;

    private Context mContext;

    public StickyListView(Context context) {
        super(context);
        this.mContext = context;
        initView();
    }

    public StickyListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initView();
    }

    public StickyListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        initView();
    }

    private void initView() {
        mMinHeaderHeight = -getResources().getDimensionPixelSize(R.dimen.header_height);

        mListViewHeaderView = LayoutInflater.from(mContext).inflate(R.layout.header_layout, null);
        addHeaderView(mListViewHeaderView);
    }

    public int getMinHeaderHeight() {
        return mMinHeaderHeight;
    }

    /**
     * 移动的距离
     *
     * @return
     */
    public float getMScrollY() {
        View c = getChildAt(0);

        if (c == null) {
            return 0;
        }

        int firstVisiblePosition = getFirstVisiblePosition();
        int top = c.getTop();

        int headerHeight = 0;
        if (firstVisiblePosition >= 1) {
            headerHeight = mListViewHeaderView.getHeight();
        }
        return -top + firstVisiblePosition * c.getHeight() + headerHeight;
    }
}
