package com.stickylayout.demo;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;

import com.stickylayout.demo.util.DataHelper;
import com.stickylayout.demo.view.StickyListView;

public class WayOneActivity extends Activity {

    private StickyListView mListView;

    private View mHeaderView;

    private int mMinHeaderHeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_way_one);

        initView();
    }

    private void initView() {
        mHeaderView = findViewById(R.id.header_view);
        initListView();
    }

    private void initListView() {
        mListView = (StickyListView) findViewById(R.id.lv_way_one);

        mMinHeaderHeight = mListView.getMinHeaderHeight();

        mListView.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, DataHelper
                .getListViewItems(30)));

        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @SuppressLint("NewApi")
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                float scrollY = mListView.getMScrollY();
                mHeaderView.setTranslationY(Math.max(-scrollY, mMinHeaderHeight));

            }
        });
    }

}
